$mobile = 810;

function animateCSS(element, animationName, callback) {
    var node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

function Toggle(list) {

    var status_li = $(".menu-b-" + list).hasClass("fa-plus");
    if (status_li) {
        $(".menu-b-" + list).removeClass("fa-plus");
        $(".menu-b-" + list).addClass("fa-angle-down");
        $(".menu-li-" + list).show('fade');
    } else {
        $(".menu-b-" + list).removeClass("fa-angle-down");
        $(".menu-b-" + list).addClass("fa-plus");
        $(".menu-li-" + list).hide('fade');
    }
}

function ToggleSB(list) {
    var status_li = $(".menu-b-" + list).hasClass("fa-plus");
    if (status_li) {
        $(".menu-b-" + list).removeClass("fa-plus");
        $(".menu-b-" + list).addClass("fa-minus");
        $(".menu-li-" + list).show('fade');
    } else {
        $(".menu-b-" + list).removeClass("fa-minus");
        $(".menu-b-" + list).addClass("fa-plus");
        $(".menu-li-" + list).hide('fade');
    }
}

function ToggleSabor(list) {
    var status_li = $(".sabor-" + list).hasClass("fa-check-square");
    if (status_li) {
        $(".sabor-" + list).removeClass("fa-check-square");
        $(".sabor-" + list).addClass("fa-square");

    } else {
        $(".sabor-" + list).removeClass("fa-square");
        $(".sabor-" + list).addClass("fa-check-square");
    }
}

function TogglePV(list) {
    $(".li-b-" + list).toggle('fade');
}

function SobrePRT(item) {
    $("#prt-1").removeClass("item-active");
    $("#prt-2").removeClass("item-active");
    $("#prt-3").removeClass("item-active");
    $(".sobre-produto-text-1").hide('fade down');
    $(".sobre-produto-text-2").hide('fade down');
    $(".sobre-produto-text-3").hide('fade down');

    $("#prt-" + item).addClass("item-active");
    $(".sobre-produto-text-" + item).show('fade up');
}

$(document).ready(function () {
    $('li.helperComplement').remove();

    //var menu
    var menuIcon = document.querySelector('.menu-toogle');
    var sideNav = document.querySelector('.side-nav');
    var btnCloseNav = document.querySelector('.close-nav');
    var overlay = document.querySelector('.overlay');
    //vars shop cart
    var aClose = document.querySelector('.aClose');
    var xClose = document.querySelector('.xClose');
    //vars filter - tela interna
    var fClose = document.querySelector('.only-mobile-filter-close');
    var fOpen = document.querySelector('.filtro-open');
    //politicas
    activeCarousel();

    //carrinho lateral
    var carrinho = (function () {
        var geral = {
            toggle_carrinho: function () {
                $('header .cart-container a').on('click', function (event) {
                    event.preventDefault();
                    $('#cart-lateral, #overlay').addClass('active');
                    showOverlay();
                });

                $('#cart-lateral .header .close').on('click', function (event) {
                    event.preventDefault();
                    $('#cart-lateral, #overlay').removeClass('active');
                    hideOverlay();
                });
            }
        }

        var desktop = {
            calculateShipping: function () {
                if ($('#search-cep input[type="text"]').val() != '') {
                    vtexjs.checkout.getOrderForm()
                        .then(function (orderForm) {
                            if (localStorage.getItem('cep') === null) {
                                var postalCode = $('#search-cep input[type="text"]').val();
                            } else {
                                var postalCode = localStorage.getItem('cep');
                            }

                            var country = 'BRA';
                            var address = {
                                "postalCode": postalCode,
                                "country": country
                            };

                            return vtexjs.checkout.calculateShipping(address)
                        })
                        .done(function (orderForm) {
                            if (orderForm.totalizers.length != 0) {
                                var value_frete = orderForm.totalizers[1].value / 100;
                                value_frete = value_frete.toFixed(2).replace('.', ',').toString();
                                $('#cart-lateral .value-frete').text('R$: ' + value_frete);

                                var postalCode = $('#search-cep input[type="text"]').val();
                                localStorage.setItem('cep', postalCode);
                                $('#search-cep input[type="text"]').val(postalCode);
                            }
                        });
                }
            },

            //APOS INSERIDO - CALCULA AO CARREGAR A PG
            automaticCalculateShipping: function () {
                $(window).load(function () {
                    if (localStorage.getItem('cep') != null && localStorage.getItem('cep') != 'undefined') {
                        $('#search-cep input[type="text"]').val(localStorage.getItem('cep'));
                        desktop.calculateShipping();
                    }
                });
            },

            //CALCULA MANUALMENTE
            calculaFrete: function () {
                //CLICK
                $('#search-cep input[type=submit]').on('click', function (e) {
                    e.preventDefault();
                    if ($('#search-cep input[type="text"]').val().length === 9) {
                        desktop.calculateShipping();
                        $('#search-cep input[type="text"]').removeClass('active');
                    } else {
                        $('#search-cep input[type="text"]').addClass('active');
                    }
                });

                //PRESS ENTER
                $('#search-cep input[type=text]').on('keypress', function (event) {
                    if (keycode == '13') {
                        if ($('#search-cep input[type="text"]').val().length === 9) {
                            var keycode = (event.keyCode ? event.keyCode : event.which);
                            desktop.calculateShipping();
                            $('#search-cep input[type="text"]').removeClass('active');
                        } else {
                            $('#search-cep input[type="text"]').addClass('active');
                        }
                    }
                });
            },

            cartLateral: function () {
                vtexjs.checkout.getOrderForm()
                    .done(function (orderForm) {
                        //REMOVE LOADING
                        $('#cart-lateral .columns').removeClass('loading');

                        //TOTAL CARRINHO
                        var quantidade = 0;
                        for (var i = orderForm.items.length - 1; i >= 0; i--) {
                            quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
                        }

                        $('header .cart-container a > span').text(quantidade);

                        //INFORMACOES DO CARRINHO && FRETE BAR
                        if (orderForm.value != 0) {
                            total_price = orderForm.value / 100;
                            faltando = 150 - total_price;
                            porcentagem = Math.round(( total_price * 100 ) / 150);
                            total_price = total_price.toFixed(2).replace('.', ',').toString();
                            faltando = faltando.toFixed(2).replace('.', ',').toString();
                            porcentagem = porcentagem.toString();
                            console.log(porcentagem);
                            $('#cart-lateral .footer .total-price').text('R$: ' + total_price);
                            $('.frete_bar').addClass('actived');
                            $('head').append('<style>.frete_bar-progress_bar:before{width:'+porcentagem+'% !important;}</style>');
                            if(porcentagem >= 100){
                                $('.frete_bar-progress_msg span').removeClass('actived');
                                $('.frete_bar-progress_msg .msg3').addClass('actived');
                            }else{
                                $('.frete_bar-progress_msg span').removeClass('actived');
                                $('.frete_bar-progress_msg .msg2').addClass('actived');
                                $('.frete_bar-progress_msg .msg2 span.faltando').text('R$ '+faltando);
                            }
                        } else {
                            $('.frete_bar').addClass('actived');
                            $('head').append('<style>.frete_bar-progress_bar:before{width:0% !important;}</style>');
                            $('.frete_bar-progress_msg span').removeClass('actived');
                            $('.frete_bar-progress_msg .msg1').addClass('actived');
                            $('#cart-lateral .footer .total-price').text('R$: 0,00');
                        }

                        if (orderForm.totalizers.length != 0) {
                            sub_price = orderForm.totalizers[0].value / 100;
                            sub_price = sub_price.toFixed(2).replace('.', ',').toString();

                            $('#cart-lateral .footer .value-sub-total, #cart-lateral .header .value-sub-total').text('R$: ' + sub_price);
                        } else {
                            $('#cart-lateral .footer .value-sub-total, #cart-lateral .header .value-sub-total').text('R$: 0,00');
                        }

                        if (orderForm.items != 0) {
                            total_items = orderForm.items.length;

                            $('#cart-lateral .header .total-items span').text(total_items + ' Itens');
                        } else {
                            $('#cart-lateral .header .total-items span').text('0 Itens');
                        }
                        //FIM - INFORMACOES DO CARRINHO

                        //ITEMS DO CARRINHO
                        $('#cart-lateral .content ul li').remove();
                        for (i = 0; i < orderForm.items.length; i++) {

                            // console.log(orderForm.items[i]);
                            priceItem = orderForm.items[i].price
                            price_item = orderForm.items[i].price / 100;
                            price_item = price_item.toFixed(2).replace('.', ',').toString();
                            promocao = orderForm.items[i].listPrice;
                            promocaoPreco = promocao / 100;
                            promocaoPreco = promocaoPreco.toFixed(2).replace('.', ',').toString();


                            var content = '';

                            content += '<li data-index="' + i + '">';
                            content += '<div class="column_1"><img src="' + orderForm.items[i].imageUrl + '" alt="' + orderForm.items[i].name + '"/></div>';

                            content += '<div class="column_2">';
                            content += '<div class="name">';
                            content += '<p>' + orderForm.items[i].name + '</p>';
                            content += '</div>';

                            content += '<div class="ft">';
                            content += '<ul>';
                            content += '<li data-index="' + i + '">';
                            content += '<div class="box-count">';
                            content += '<a href="" class="count count-down"><img src="/arquivos/menos.png"></a>';
                            content += '<input type="text" value="' + orderForm.items[i].quantity + '" />';
                            content += '<a href="" class="count count-up"><img src="/arquivos/mais.png"></a>';
                            content += '</div>';
                            content += '</li>';

                            if (promocao === priceItem) {
                                content += '<li class="price">';
                                content += '<p>R$: ' + price_item + '</p>';
                                content += '</li>';
                                content += '<ul>';

                                content += '</div>';
                                content += '</div>';

                                content += '<span class="removeUni">x</span>';
                                content += '</li>';
                            } else {

                                content += '<li class="price">';
                                content += '<p class="bestPrice">De: R$: ' + promocaoPreco + '</p>';
                                content += '<p>R$: ' + price_item + '</p>';
                                content += '</li>';
                                content += '<ul>';

                                content += '</div>';
                                content += '</div>';

                                content += '<span class="removeUni">x</span>';
                                content += '</li>';
                            }

                            $('#cart-lateral .content > ul').append(content);
                        }
                        //FIM - ITEMS DO CARRINHO


                    });
            },

            changeQuantity: function () {
                $(document).on('click', '#cart-lateral .count', function (e) {
                    e.preventDefault();

                    $('#cart-lateral .columns').addClass('loading');

                    var qtd = $(this).siblings('input[type="text"]').val();
                    if ($(this).hasClass('count-up')) {
                        qtd++
                        $(this).siblings('input[type="text"]').removeClass('active');
                        $(this).siblings('input[type="text"]').val(qtd);
                    } else if ($(this).hasClass('count-down')) {
                        if ($(this).siblings('input[type="text"]').val() != 1) {
                            qtd--
                            $(this).siblings('input[type="text"]').val(qtd);
                        } else {
                            //ALERTA 0 USUARIO QUANTIDADE NEGATIVA
                            $(this).siblings('input[type="text"]').addClass('active');
                        }
                    }

                    var data_index = $(this).parents('li').data('index');
                    var data_quantity = $(this).parents('li').find('.box-count input[type="text"]').val();

                    vtexjs.checkout.getOrderForm()
                        .then(function (orderForm) {
                            var total_produtos = parseInt(orderForm.items.length);
                            vtexjs.checkout.getOrderForm()
                                .then(function (orderForm) {
                                    var itemIndex = data_index;
                                    var item = orderForm.items[itemIndex];

                                    var updateItem = {
                                        index: data_index,
                                        quantity: data_quantity
                                    };

                                    return vtexjs.checkout.updateItems([updateItem], null, false);
                                })
                                .done(function (orderForm) {
                                    desktop.cartLateral();
                                });
                        });
                });
            },

            removeItems: function () {
                $(document).on('click', '#cart-lateral .removeUni', function () {

                    var data_index = $(this).parents('li').data('index');
                    var data_quantity = $(this).siblings('li').find('.box-count input[type="text"]').val();

                    vtexjs.checkout.getOrderForm()
                        .then(function (orderForm) {
                            var itemIndex = data_index;
                            var item = orderForm.items[itemIndex];
                            var itemsToRemove = [{
                                "index": data_index,
                                "quantity": data_quantity,
                            }]
                            return vtexjs.checkout.removeItems(itemsToRemove);
                        })
                        .done(function (orderForm) {
                            desktop.cartLateral();
                        });
                });
            },

            removeAllItems: function () {
                $('#cart-lateral .clear').on('click', function () {
                    vtexjs.checkout.removeAllItems()
                        .done(function (orderForm) {
                            //ATUALIZA O CARRINHO APÓS ESVAZIAR
                            desktop.cartLateral();
                        });
                });
            },

            btn_buy: function () {
                window.alert = function () {
                    //OPEN CART
                    showOverlay();
                    $('header .cart-container a').trigger('click');
                    desktop.cartLateral();
                }
            },

            openCart: function () {
                $('#overlay').on('click', function () {
                    if ($('#cart-lateral').hasClass('active')) {
                        $('#cart-lateral, #overlay').removeClass('active');
                    }
                });
            }
        };

        var prateleira = {
            buy_button: function () {
                $('.produtos-card .btn.btn-compra').on('click', function (e) {
                    e.preventDefault();

                    $(this).addClass('disabled');

                    var id = $(this).parents('.produtos-card').data('id');
                    var quantidade = parseInt($(this).siblings('input#qtd').val());
                    var value = quantidade;

                    if (value === 0) {
                        var value2 = quantidade + 1;
                        $(this).siblings('input').val(value2);
                    } else {
                        var value2 = quantidade;
                        $(this).siblings('input').val(value2);
                    }

                    $(this).siblings('input').val(value);

                    vtexjs.catalog.getProductWithVariations(id).done(function (product) {
                        var item = {
                            id: product.skus[0].sku,
                            quantity: value2,
                            seller: '1'
                        };

                        vtexjs.checkout.getOrderForm()
                            .done(function (orderForm) {
                                vtexjs.checkout.getOrderForm()
                                vtexjs.checkout.addToCart([item], null, 1).done(function (orderForm) {

                                    //OPEN CART
                                    showOverlay();
                                    $('header .cart-container > a').trigger('click');
                                    desktop.cartLateral();

                                    $('.produtos-card .btn.btn-compra').removeClass('disabled');
                                });
                            });
                    });
                });
            },

            btn_qtd: function () {
                // $('#mais, #menos').show();

                //QUANTIDADE EM ESTOQUE
                function max() {
                    $('.produtos-card').each(function (index, card) {
                        vtexjs.catalog.getProductWithVariations($(card).data('id')).done(function (product) {
                            $.each(product.skus, function (index, item) {
                                //PEGA O PRIMEIRO EM ESTOQUE
                                if (item.available === true) {
                                    $(card).find('input#qtd').attr('max', item.availablequantity);
                                }
                            });
                        });
                    });
                };
                max();

                //CONTADOR
                function count() {
                    $('.produtos-card .btn_qtd').on('click', function (e) {
                        e.preventDefault();

                        //QUANTIDADE ATUAL
                        var input = $(this).siblings('input');

                        //QUANTIDADE MÁXIMA
                        var max = parseInt(input.attr('max'));

                        if ($(this).hasClass('mais')) {
                            var qtd = parseInt(input.attr('value'));

                            if (qtd < max && qtd < 100) {
                                qtd++;
                            }
                        } else if ($(this).hasClass('menos')) {
                            var qtd = parseInt(input.attr('value'));

                            if (qtd > 0) {
                                qtd--;
                            }
                        }

                        input.attr('value', qtd);
                    });
                };
                count();
            }
        };

        var departamento = {
            smart_research: function () {
                $('.search-multiple-navigator input[type="checkbox"]').vtexSmartResearch({
                    shelfCallback: function () {
                        console.log('shelfCallback');
                    },
    
                    ajaxCallback: function () {
                        console.log('ajaxCallback');
                        stock1();
                        // stock2();
                        prateleira.buy_button();
                        prateleira.btn_qtd();
                    }
                });   
            }
        };

        geral.toggle_carrinho();

        desktop.automaticCalculateShipping();
        desktop.calculaFrete();
        desktop.cartLateral();
        desktop.changeQuantity();
        desktop.removeItems();
        desktop.removeAllItems();
        desktop.btn_buy();
        desktop.openCart();

        prateleira.buy_button();
        prateleira.btn_qtd();

        departamento.smart_research();
    })();
    //carrinho lateral

    var institucional = (function () {
        var ambos = {
            search_lojas: function () {
                $('.input-search-loja.btn-rounded').keypress(function (el) {
                    var thisText = $(el).text();
                    thisText = thisText.toLowerCase();
                    string = string.toLowerCase();

                    if (thisText.indexOf(string) != -1) {
                        $(el).css('display', 'block');
                    }
                });
            }
        };

        ambos.search_lojas();
    })();

    var geral = (function () {
        var ambos = {
            logado: function () {
                $.ajax({
                    url: '/api/vtexid/pub/authenticated/user',
                    type: 'GET'
                }).done(function (user) {
                    // console.log(user);

                    if (user === null || user.user === 'suporte@sandersdigital.com.br') {
                        //CLIENTE OFFLINE
                    } else {
                        //CLIENTE ONLINE
                        $('header .login-container ul li a[href="/no-cache/user/logout"]').removeClass('dis-none');
                        $('header .login-container ul li a[href="/_secure/account#/orders"]').removeClass('dis-none');
                    }
                });
            },

            //quantidade no carrinho
            qtd: function () {
                vtexjs.checkout.getOrderForm()
                    .done(function (orderForm) {
                        var quantidade = 0;
                        for (var i = orderForm.items.length - 1; i >= 0; i--) {
                            quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
                        }

                        $('header .cart-container a > span').text(quantidade);
                    });
            },

            number: function () {
                $(document).on('keyup', 'input[type="number"]', function () {
                    var _this = $(this);
                    var min = parseInt(_this.attr('min')); // if min attribute is not defined, 1 is default
                    var max = parseInt(_this.attr('max')) || 100; // if max attribute is not defined, 100 is default
                    var val = parseInt(_this.val()) || (min - 1); // if input char is not a number the value will be (min - 1) so first condition will be true
                    if (val > max)
                        _this.val(max);
                });
            }
        };

        var mobile = {
            busca: function () {
                $('header .busca-container').on('click', function () {
                    $('header fieldset.busca').toggleClass('active');
                });
            },

            menu: function () {
                $('.side-nav ul .nav-section-item').on('click', function (e) {
                    if ($(this).next('.submenu').length) {
                        e.preventDefault();
                        $(this).toggleClass('active');
                        $(this).next('.submenu').slideToggle();
                    }
                });

                $('.todos_departamentos-button').on('click', function (e) {
                    e.preventDefault();
                    $('.menu_default').addClass('active');
                    $('.todos_departamentos-mobile').addClass('active');
                });

                $('.todos_departamentos-button-back').on('click', function (e) {
                    e.preventDefault();
                    $('.menu_default').removeClass('active');
                    $('.todos_departamentos-mobile').removeClass('active');
                });
            },

            institucionalFooter: function () {
                $('.footer .menus .menu-content h4').on('click', function (e) {
                    e.preventDefault();


                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                        $(this).next('ul.menu').slideToggle();
                    } else {
                        $(this).next('ul.menu').slideToggle();
                        $(this).addClass('active');
                    }
                });
            }
        };

        ambos.logado();
        ambos.qtd();
        ambos.number();

        if ($('body').width() < $mobile) {
            mobile.busca();
            mobile.menu();
            mobile.institucionalFooter();
        }
    })();

    //product
    if ($('body').hasClass('produto')) {
        function count() {
            $('.produto-qtd-itens .botao').on('click', function () {
                var value = $('.produto-qtd-itens .produto-simple-btn').val();

                if ($(this).hasClass('produto-more-btn')) {
                    var qtd_produtos = parseInt(value);
                    qtd_produtos++;
                } else if ($(this).hasClass('produto-minus-btn')) {
                    var qtd_produtos = parseInt(value);

                    if (qtd_produtos > 1) {
                        qtd_produtos--;
                    }
                }

                $('.produto-qtd-itens .produto-simple-btn').val(qtd_produtos);
                $('.buy-in-page-quantity').trigger('quantityChanged.vtex', [skuJson.productId, qtd_produtos]);
            });
        }
        count();

        //ALTERAR MANUALMENTE
        $('.produto-qtd-itens .produto-simple-btn').change(function () {
            var qtd = parseInt($(this).val());
            $('.buy-in-page-quantity').trigger('quantityChanged.vtex', [skuJson.productId, qtd]);
        });
    }
    //fim produto   
    
    // POP BEBIDAS
    var PopupBebidas = (function(){
        var ambos = {
            showPopup: function () {
                $('.popup_bebidas').css('display', 'block');
                $('.popup_bebidas .confirmed').click(function (e) { 
                    $('.popup_bebidas').remove();
                });
            }
        }
        var GetUrl = window.location.href;
        if(GetUrl.includes('bebidas')){
            ambos.showPopup();
        }

    })()
    // POP BEBIDAS
    $('.icon-plus-sign, .icon-minus-sign').click(function (e) { 
        console.log('ok');
    }); 
    
    //Stock
    function stock1() { // Trento Chocolate normal amendoim
        $.ajax({
            url: '/api/catalog_system/pub/products/search?fq=productId:54600130',
            type: 'GET'
        }).
            done(function (response, status) {
                // console.log(response);
                var product_item = $(".performa-vitrine-item[data-id='527493084']");
                $('.performa-vitrine-item[data-id="527493084"]').each(function (index, element) {
                    if (!$(this).find(".performa-price-area-vitrine").hasClass('stock_ok')) {
                        product_item.find(".tns-lazy-img").css('height', '289px');
                        product_item.find(".performa-price-area-vitrine").append('<p style="font-size: 14px;font-weight: 600;color:red;">Restam <span style="background: #f5f5f5; padding: 8px;">' + response[0].items[0].sellers[0].commertialOffer.AvailableQuantity + '</span> unidades!</p>');
                        product_item.find(".performa-price-area-vitrine").addClass('stock_ok');
                    }
                });
                $('.produtos-card[data-id="54600130"]').each(function (index, element) {
                    if (!$(this).find(".produto-info").hasClass('stock_ok')) {
                        var product_item = $(".produtos-card[data-id='54600130']");
                        product_item.find(".produto-img").css('height', '289px');
                        product_item.find(".produto-info").append('<p style="font-size: 14px;font-weight: 600;color:red;">Restam <span style="background: #f5f5f5; padding: 8px;">' + response[0].items[0].sellers[0].commertialOffer.AvailableQuantity + '</span> unidades!</p>');
                        product_item.find(".produto-info").addClass('stock_ok');
                    }
                });
                if ($('.productReference').hasClass('54600130')) {
                    if (!$('.main-produtos').hasClass('stock_ok')) {
                        $('.main-produtos').find(".plugin-preco").append('<p style="font-size: 14px;font-weight: 600;color:red;">Restam <span style="background: #f5f5f5; padding: 8px;">' + response[0].items[0].sellers[0].commertialOffer.AvailableQuantity + '</span> unidades!</p>');
                        $('.main-produtos').addClass('stock_ok');
                    }
                }
            });
    };

    function stock2() {// Trento Chocolate branco amendoim
        $.ajax({
            url: '/api/catalog_system/pub/products/search?fq=productId:57031366',
            type: 'GET'
        }).
            done(function (response, status) {
                // console.log(response);
                var product_item = $(".performa-vitrine-item[data-id='8951743002']");
                $('.performa-vitrine-item[data-id="8951743002"]').each(function (index, element) {
                    if (!$(this).find(".performa-price-area-vitrine").hasClass('stock_ok')) {
                        product_item.find(".tns-lazy-img").css('height', '289px');
                        product_item.find(".performa-price-area-vitrine").append('<p style="font-size: 14px;font-weight: 600;color:red;">Restam <span style="background: #f5f5f5; padding: 8px;">' + response[0].items[0].sellers[0].commertialOffer.AvailableQuantity + '</span> unidades!</p>');
                        product_item.find(".performa-price-area-vitrine").addClass('stock_ok');
                    }
                });
                $('.produtos-card[data-id="57031366"]').each(function (index, element) {
                    if (!$(this).find(".produto-info").hasClass('stock_ok')) {
                        var product_item = $(".produtos-card[data-id='57031366']");
                        product_item.find(".produto-img").css('height', '289px');
                        product_item.find(".produto-info").append('<p style="font-size: 14px;font-weight: 600;color:red;">Restam <span style="background: #f5f5f5; padding: 8px;">' + response[0].items[0].sellers[0].commertialOffer.AvailableQuantity + '</span> unidades!</p>');
                        product_item.find(".produto-info").addClass('stock_ok');
                    }
                });
                if ($('.productReference').hasClass('57031366')) {
                    if (!$('.main-produtos').hasClass('stock_ok')) {
                        $('.main-produtos').find(".plugin-preco").append('<p style="font-size: 14px;font-weight: 600;color:red;">Restam <span style="background: #f5f5f5; padding: 8px;">' + response[0].items[0].sellers[0].commertialOffer.AvailableQuantity + '</span> unidades!</p>');
                        $('.main-produtos').addClass('stock_ok');
                    }
                }
            });
    };
    
    function stock3() { //Trento Chocolate dark amendoim
        $.ajax({
            url: '/api/catalog_system/pub/products/search?fq=productId:57032380',
            type: 'GET'
        }).
        done(function (response, status) {
            console.log(response);

            $('.performa-vitrine-item[data-id="6571758008"]').each(function (index, element) {
                if(!$(this).find(".performa-price-area-vitrine").hasClass('stock_ok')){
                    var product_item = $(".performa-vitrine-item[data-id='6571758008']");
                    product_item.find(".tns-lazy-img").css('height', '289px');
                    product_item.find(".performa-price-area-vitrine").append('<p style="font-size: 14px;font-weight: 600;color:red;">Restam <span style="background: #f5f5f5; padding: 8px;">'+ response[0].items[0].sellers[0].commertialOffer.AvailableQuantity +'</span> unidades!</p>');
                    product_item.find(".performa-price-area-vitrine").addClass('stock_ok');
                }
            });
            $('.produtos-card[data-id="57032380"]').each(function (index, element) {
                if(!$(this).find(".produto-info").hasClass('stock_ok')){
                    var product_item = $(".produtos-card[data-id='57032380']");
                    product_item.find(".produto-img").css('height', '289px');
                    product_item.find(".produto-info").append('<p style="font-size: 14px;font-weight: 600;color:red;">Restam <span style="background: #f5f5f5; padding: 8px;">'+ response[0].items[0].sellers[0].commertialOffer.AvailableQuantity +'</span> unidades!</p>');
                    product_item.find(".produto-info").addClass('stock_ok');
                }
            });
            if($('.productReference').hasClass('57032380')){
                if(!$('.main-produtos').hasClass('stock_ok')){
                    $('.main-produtos').find(".plugin-preco").append('<p style="font-size: 14px;font-weight: 600;color:red;">Restam <span style="background: #f5f5f5; padding: 8px;">'+ response[0].items[0].sellers[0].commertialOffer.AvailableQuantity +'</span> unidades!</p>');
                    $('.main-produtos').addClass('stock_ok');
                }
            }

        });
    };
    var times = 0;
    var intervalID = setInterval(function () {
        stock1();
        stock2();
        stock3();
        if (times === 5) {
            window.clearInterval(intervalID);
        }
        times++
    }, 1000);

    //Fim Stock

    //departamento
    if ($('body').hasClass('departamento')) {
        function result() {
            $('#result').html('Resultados: ' + $('body > div.container > div > div.column.column_2 > div > p:nth-child(1) > span.resultado-busca-numero > span.value').text());
        }
        result();

        function flags() {
            //ADD
            $(document).on('change', '.search-multiple-navigator label input', function () {
                var thisName = $(this).parent().text(),
                    thisClass = $(this).parent().attr('title'),
                    categoriaSelecionada = '<li data-name="' + thisClass + '"><p>' + thisName + '</p><span>x</span></li>';

                if ($(this).parent().hasClass('sr_selected')) {
                    $('.flags ul').append(categoriaSelecionada);
                } else {
                    $('.flags ul li[data-name="' + thisClass + '"]').remove();
                }
            });

            //REMOVE
            $(document).on('click', '.flags li', function (e) {
                e.preventDefault();
                $('.search-multiple-navigator label[title="' + $(this).data('name') + '"]').trigger('click');
            });

            //CLEAR
            $('#clear').on('click', function (e) {
                e.preventDefault();
                $('.flags .content ul li').trigger('click');
            });
        }
        flags();
    }
    //fim - departamento

    //TIPBAR - MOBILE HOME
    if ($(window).width() < 768) {

        $('.banner-regua .banner-regua-content').slick({
            infinite: true,
            speed: 800,
            autoplay: true,
            slidesToShow: 1,
            arrows: false,
            dots: false
        });

    }

    //BANNER PRINCIPAL - HOME
    $('.slides-content ul').slick({
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        arrows: true,
        dots: true
    });

    function showOverlay() {
        overlay.style.display = 'block';
    }

    function hideOverlay() {
        overlay.style.display = 'none';
    }

    function closeContinueBuy() {
        $('#continue_buy .close').on('click', function (e) {
            e.preventDefault();

            $('#continue_buy').removeClass('active');
            hideOverlay();
        });
    }
    closeContinueBuy();

    function showSideNav() {
        sideNav.classList.toggle('active');
        document.body.style.overflow = 'hidden';
        showOverlay();
    }

    function closeSideNav(e) {
        sideNav.classList.toggle('active');
        document.body.style.overflow = '';
        hideOverlay();
    }

    function CloseCart() {
        animateCSS('.car-nav', 'slideOutDown', function () {
            $(".car-nav").hide();
            $(".car-main").hide();
        })
    }

    function OpenFilter() {
        $(".menus-for-mobile").show();
        animateCSS('.menus-for-mobile', 'slideInUp');
    }

    function CloseFilter() {
        animateCSS('.menus-for-mobile', 'slideOutDown', function () {
            $(".menus-for-mobile").hide();
        })
    }

    menuIcon.addEventListener('click', showSideNav);
    btnCloseNav.addEventListener('click', closeSideNav);
});

function activeCarousel() {
    var widthDevice = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (widthDevice >= 768) {
        activeCarouselsProdutos();
    }
}

function activeCarouselsProdutos() {
    $('.favoritos-produtos ul').slick({
        infinite: true,
        speed: 500,
        autoplay: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        variableWidth: false,
        responsive: [{
            breakpoint: 1024,
            settings: {
                variableWidth: true
            }
        }]
    });

    $('.descontinho-especial-produtos ul').slick({
        infinite: true,
        speed: 500,
        autoplay: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        variableWidth: false,
        responsive: [{
            breakpoint: 1024,
            settings: {
                variableWidth: true
            }
        }]
    });
}