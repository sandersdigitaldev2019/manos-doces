

$(document).ready(function () {
    vtexjs.checkout.getOrderForm().done(function (orderForm) {
        console.log(orderForm.totalizers[0].value);
        if (orderForm.totalizers[0].value != 0) {
            total_price = orderForm.totalizers[0].value / 100;
            faltando = 150 - total_price;
            porcentagem = Math.round(( total_price * 100 ) / 150);
            faltando = faltando.toFixed(2).replace('.', ',').toString();
            porcentagem = porcentagem.toString();
            $('.frete_bar').addClass('actived');
            $('head').append('<style>.nav-sections nav > ul > li .submenu{ top:188px !important; }.frete_bar-progress_bar:before{width:'+porcentagem+'% !important;}</style>');
            if(porcentagem >= 100){
                $('.frete_bar-progress_msg span').removeClass('actived');
                $('.frete_bar-progress_msg .msg3').addClass('actived');
            }else{
                $('.frete_bar-progress_msg span').removeClass('actived');
                $('.frete_bar-progress_msg .msg2').addClass('actived');
                $('.frete_bar-progress_msg .msg2 span.faltando').text('R$ '+faltando);
            }
        } else {
            $('.frete_bar').addClass('actived');
            $('head').append('<style>.nav-sections nav > ul > li .submenu{ top:188px !important; }.frete_bar-progress_bar:before{width:0% !important;}</style>');
            $('.frete_bar-progress_msg span').removeClass('actived');
            $('.frete_bar-progress_msg .msg1').addClass('actived');
            $('.frete_bar-add').addClass('actived');
        }
    });
    $(window).on('orderFormUpdated.vtex', function (evt, orderForm) {
        if (orderForm.totalizers[0].value != 0) {
            total_price = orderForm.totalizers[0].value / 100;
            faltando = 150 - total_price;
            porcentagem = Math.round(( total_price * 100 ) / 150);
            faltando = faltando.toFixed(2).replace('.', ',').toString();
            porcentagem = porcentagem.toString();
            $('.frete_bar').addClass('actived');
            $('head').append('<style>.nav-sections nav > ul > li .submenu{ top:188px !important; }.frete_bar-progress_bar:before{width:'+porcentagem+'% !important;}</style>');
            if(porcentagem >= 100){
                $('.frete_bar-progress_msg span').removeClass('actived');
                $('.frete_bar-progress_msg .msg3').addClass('actived');
            }else{
                $('.frete_bar-progress_msg span').removeClass('actived');
                $('.frete_bar-progress_msg .msg2').addClass('actived');
                $('.frete_bar-progress_msg .msg2 span.faltando').text('R$ '+faltando);
            }
        } else {
            $('.frete_bar').addClass('actived');
            $('head').append('<style>.nav-sections nav > ul > li .submenu{ top:188px !important; }.frete_bar-progress_bar:before{width:0% !important;}</style>');
            $('.frete_bar-progress_msg span').removeClass('actived');
            $('.frete_bar-progress_msg .msg1').addClass('actived');
            $('.frete_bar-add').addClass('actived');
        };
    });

});
