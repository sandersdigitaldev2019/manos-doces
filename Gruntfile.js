module.exports = function (grunt) {

  grunt.initConfig({

    // uglify
    uglify: {
      options: {
        // mangle : false
      },

      my_target: {
        files: {
          'arquivos/app.min.js': 'arquivos/app_uglify.js',
        }
      }
    },
    // uglify

    //sass
    sass: {
      dist: {
        files: {
          'assets/css/style.css': 'assets/css/sass/style.scss'
        }
      }
    },
    //sass

    // concat
    concat: {
      options: {
        separator: '',
      },
      dist: {
        src: ['assets/css/sprite.scss', 'assets/css/style.css'],
        dest: 'assets/css/all/all.css',
      },
    },
    // concat


    // css min
    cssmin: {
      combine: {
        files: {
          'arquivos/all.min.css': ['assets/css/all/all.css']
        }
      }
    },
    // css min

    // strip comments
    comments: {
      js: {
        options: {
          singleline: true,
          multiline: true
        },
        src: ['arquivos/app.min.js']
      },
      css: {
        options: {
          singleline: true,
          multiline: true
        },
        src: ['arquivos/all.min.css']
      }
    },
    // strip comments

    // watch
    watch: {
      dist: {
        files: [
          'assets/js/**/*',
          'assets/css/**/*',
          'assets/img/**/*',
          'arquivos/app_uglify.js'
        ],

        tasks: ['sass', 'concat', 'cssmin', 'uglify', 'comments']
      }
    }
    // watch

  });


  // Plugins do Grunt
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-svg-sprite');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-stripcomments');
  grunt.loadNpmTasks('grunt-contrib-watch');


  // Tarefas que serão executadas
  grunt.registerTask('default', ['sass', 'concat', 'cssmin', 'uglify', 'comments']);

  // Tarefa para Watch
  grunt.registerTask('w', ['watch']);

};